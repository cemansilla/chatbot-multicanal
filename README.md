# Dependencias
pip install telebot
pip install pyTelegramBotAPI
pip install chatterbot
pip install chatterbot-corpus

# Configuraciones

## Telegram
- Obtener token https://core.telegram.org/bots#6-botfather
- Cargar el token en `Telegram/config.py`

## Twitter
- Crear cuenta de desarrollador https://developer.twitter.com/
- Crear APP https://developer.twitter.com/en/apps