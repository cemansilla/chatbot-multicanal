# coding=utf-8

import telebot
from telebot import types
from chatterbot import ChatBot
from chatterbot.trainers import ChatterBotCorpusTrainer
from chatterbot.trainers import ListTrainer
from . import config

class Telegram():
  def __init__(self, TOKEN):
    self.TOKEN = TOKEN

    def listener(messages):
      for m in messages:
        if m.content_type == "text":
          print(m.text)

    bot = telebot.TeleBot(self.TOKEN)
    bot.set_update_listener(listener)

    # Implementación bot conversacional (Chatterbot)
    def bot_conversational(message):
      chatbot = ChatBot(config.TELEGRAM_BOT_NAME)

      # El entrenamiento se usa la primera vez, no dejarlo porque se ejecutaría tras cada petición 
      # Opción 1: a través de un corpus remoto (https://github.com/gunthercox/chatterbot-corpus/tree/master/chatterbot_corpus)
      #trainer = ChatterBotCorpusTrainer(chatbot)
      #trainer.train("chatterbot.corpus.spanish")

      # Opción 2: a través de un corpus local
      trainer = ChatterBotCorpusTrainer(chatbot)
      trainer.train("./data/custom_corpus/custom.corpus.yml")

      #Opción 3: a través de una lista de sentencias
      """
      conversation = [
        "Hello",
        "Hi there!",
        "How are you doing?",
        "I'm doing great.",
        "That is good to hear",
        "Thank you.",
        "You're welcome."
      ]
      trainer = ListTrainer(chatbot)
      trainer.train(conversation)      
      """

      respuesta = chatbot.get_response(message)
      return str(respuesta)

    # Primer contacto o respuesta a comandos (comienzan con /)
    @bot.message_handler(commands = ["help", "start"])
    def enviar_mensaje(message):
      bot.reply_to(message, "Hola, soy un bot :)")

    # Procesamiento de mensajes en general
    @bot.message_handler(func=lambda message: True, content_types=['text'])
    def mensaje(message):
      respuesta = bot_conversational(message.text)
      bot.reply_to(message, respuesta)

    bot.polling()